#-------------------------------------------------------------------------------
# Name:        Operators
# Purpose:
#
# Author:      Muhammad Hasan
#
# Created:     09/07/2019
# Copyright:   (c) Muhammad Hasan 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import operator

my_list = [1,3,5,7,9,11,13,15]
num_1 = 10
num_2 = 15

def operatorGetItem():
    # getitem(object, position)
    print(operator.getitem(my_list, 2)) #--> 5
    return


def operatorSetItem():
    # setitem(object, position, value)
    operator.setitem(my_list, 4, 11 )
    print(my_list) #--> [1, 3, 5, 7, 11, 11, 13, 15]
    return


def operatorDeleteItem():
    # delitem(object, position, value)
    operator.delitem(my_list, 4)
    print(my_list) #--> [1, 3, 5, 7, 11, 13, 15]


def operatorGroupGetItem():
    # getitem(object, slice(start index, end index))
    print(operator.getitem(my_list,slice(1,3))) #--> [3, 5]
    return


def operatorGroupSetItem():
    # setitem(object, position, value)
    operator.setitem(my_list, slice(1,3), [33,55] )
    print(my_list) #--> [1, 33, 55, 7, 11, 13, 15]
    return


def operatorGroupDeleteItem():
    # delitem(object, slice(start,end))
    operator.delitem(my_list, slice(1,3))
    print(my_list)  #--> [1, 7, 11, 13, 15]


def operatorConcate():
    # concat(obj1, obj2)
    print(operator.concat('Hello!', ' World')) #--> Hello! World


def operatorContains():
    # contains(from, search)
    print(operator.contains('ABCDEF', 'DE') ) #--> True


def operatorAdd():
    print( operator.add(my_list, [2,4]) )    #--> [1, 3, 5, 7, 9, 11, 13, 15, 2, 4]
    print( operator.add(num_1, num_2) )      #--> 25
    print( operator.add( (1,2), (3,4) ) )    #--> (1, 2, 3, 4)
    print( operator.add( 'Add ', 'String') ) #--> Add String

    # NB: operator.add() doesn't work with python set and dictionary.


def main():
    print(my_list) #--> [1, 3, 5, 7, 9, 11, 13, 15]



if __name__ == '__main__':
    main()
    operatorAdd()

