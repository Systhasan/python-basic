#-------------------------------------------------------------------------------
# Name:        nester for loop
# Purpose:
#
# Author:      Muhammad hasan
#
# Created:     13/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
# Description: Print character (F) using * symbol.
#-------------------------------------------------------------------------------

def main():
    number_of_x = [5,1,5,1,1]
    for x in number_of_x:
        for y in range(0, x):
            print('X', end='')
        print('')

if __name__ == '__main__':
    main()
