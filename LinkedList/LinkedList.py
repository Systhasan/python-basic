class Node:
    def __init__(self, value=None, next_node=None):
        self.data = value
        self.next_node = next_node

    def get_data(self):
        return self.data

    def set_data(self, value):
        self.data = value

    def set_next_node(self, value):
        self.next_node = value

    def get_next_node(self):
        return self.next_node


class LinkedList:
    def __init__(self, head=None):
        self.head = head
        self.size = 0

    def addNode(self, data):
        # set data to node
        new_node = Node(data)
        # set head node pointer
        new_node.set_next_node(self.head)
        # node object
        self.head = new_node
        self.size += 1
        return True

    def searchNode(self, data):
        current = self.head
        found = False
        while current and found is False:
            if current.get_data() == data:
                found = True
            else:
                current = current.get_next_node()
        if current is None:
            raise ValueError("Data not in List")
        # return node object
        return current

    def printNode(self):
        current = self.head
        while current:
            print(current.get_data())
            current = current.get_next_node()


print('Started ...')
linked_list_1 = LinkedList()
linked_list_1.addNode(102)
linked_list_1.addNode(104)
linked_list_1.addNode(106)
linked_list_1.addNode(108)
linked_list_1.printNode()

value = linked_list_1.searchNode(106)
# print(value.__dict__)
# print(linked_list_1.searchNode(106).next_node.next_node.next_node)
# print(value.next_node.next_node.next_node)
print('Stop ...')
print(type(value))
