#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Administrator
#
# Created:     25/06/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    pass

if __name__ == '__main__':
    main()

    def to_upper_case(s):
        return str(s).upper() # for  string uppercase

    def print_iterator(it):
        for x in it:
            print(x, end=' ')
        print('')  # for new line


    tuple_iterator = tuple( map(to_upper_case, ['String', 'A'] ) )
    print(tuple_iterator)

    list_iterator = list( map(to_upper_case, ['String', 'A'] ) )
    print(list_iterator)

    set_iterator = set( map(to_upper_case, ['String', 'A'] ) )
    print(set_iterator)


    list_numbers = [1, 2, 3, 4]
    tuple_numbers = (5, 6, 7, 8)
    map_iterator = map(lambda x, y: x*y , list_numbers, tuple_numbers)
    print_iterator(map_iterator)