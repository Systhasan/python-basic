#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Administrator
#
# Created:     13/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    numbers = [2,2,4,5,6,7,8,6,10,5]
    uniques = []
    for number in numbers:
        if number not in uniques:
            uniques.append(number)
    print(uniques)


if __name__ == '__main__':
    main()
