#-------------------------------------------------------------------------------
# Name:        Python Inheritance.
# Purpose:
#
# Author:      Muhammad Hasan
#
# Created:     14/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

# Parent Class.
class Auto:
    def __init__(self, name, model, color, price, mileage):
        self.name    = name
        self.model   = model
        self.color   = color
        self.price   = price
        self.mileage = mileage

    def getName(self):
        return self.name

    def getModel(self):
        return self.model

    def getColor(self):
        return self.color

    def getPrice(self):
        return self.price

    def getMileage(self):
        return self.mileage


# Child Class inherite parent class.
class Car(Auto):
    def __init__(self, name, model, color, price, mileage):
        Auto.__init__(self, name, model, color, price, mileage)

    def setOwnername(self, owner):
        self.owner = owner

    def getOwnerName(self):
        try:
            return self.owner
        except AttributeError:
            return 'Not Define'


# Child Class inherite parent class
class Bike(Auto):
    def __init__(self, name, model, color, price, mileage):
        Auto.__init__(self, name, model, color, price, mileage)


audi = Car('Audi', 'Audi-01', 'White', 2200000, '30 km/h')
print(audi.getColor())

yamaha = Bike('Yamaha', 'Version 1', 'Red', 2000000, '35 km/h')
print(yamaha.getColor())

