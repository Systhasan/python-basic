n = int(input())
arr = []
for i in range(n):
    arr.append(int(input()))

max = max(arr[0], arr[1])
secondmax = min(arr[0], arr[1])

for i in range(2, len(arr)):
    if arr[i] > max  :
        secondmax = max
        max = arr[i]
    elif arr[i] > secondmax and arr[i] != max:
        secondmax = arr[i]
print(secondmax)