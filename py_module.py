#-------------------------------------------------------------------------------
# Name:        py_module
# Purpose:
#
# Author:      Muhammad Hasan
#
# Created:     14/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

# Load all methods from converter Module
from Converter import *
# Load all methods form quantity Module
import quantity
# Load Only km_to_meter() method from length Module
from length import km_to_meter



if __name__ == '__main__':
    print(f'10 lbs = {lbs_to_kg(10)} Kg.')
    print(f'10 km ={km_to_meter(10)} Meters.')
    print(f'12 liter ={quantity.liter_to_cubicMetre(12)} Cubic Metre.')
    print(f'15 liter ={quantity.liter_to_gallons(15)} Galoon.')
