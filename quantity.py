#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Administrator
#
# Created:     14/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def liter_to_cubicMetre(liter):
    return liter*0.001

def liter_to_gallons(liter):
    return liter*0.264
