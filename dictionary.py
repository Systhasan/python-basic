#-------------------------------------------------------------------------------
# Name:        Dictionary
# Purpose:
#
# Author:      Administrator
#
# Created:     13/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    numbers = {
        '1' : 'One',
        '2' : 'Two',
        '3' : 'Three',
        '4' : 'Four',
        '5' : 'Five',
    }
    get_number = input('Type Number: ')
    output = ""
    for x in get_number:
        output += numbers.get(x, "!") + " "
    print(output)

if __name__ == '__main__':
    main()
