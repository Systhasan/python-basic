#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Administrator
#
# Created:     09/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

class CSEStudent:

    class_name='CSE'    # static/class variable

    def __init__(self, stdname, stdroll, section):
        self.std_name    = stdname  # instance variable
        self.std_roll    = stdroll  # instance variable
        self.std_section = section  # instance variable

    def getStudentName(self):
        return self.std_name

    def getStudentRoll(self):
        return self.std_roll

    def getStudentSection(self):
        return self.std_section



if __name__ == '__main__':

    std_a = CSEStudent('Muhammad', '101', 'B.Sc.')
    std_b = CSEStudent('Hasan', '102', 'M.Sc.')

    print(CSEStudent.class_name)    # get class static variable.


    print('Student Name:',std_a.getStudentName() )
    print('Class: ',std_a.class_name )
    print('Roll: ',std_a.getStudentRoll() )
    print('Section: ',std_a.getStudentSection() )

    print('Student Name:',std_b.getStudentName() )
    print('Class: ',std_b.class_name )
    print('Roll: ',std_b.getStudentRoll() )
    print('Section: ',std_b.getStudentSection() )



