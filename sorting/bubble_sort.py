def bubble_sort(arr, start, end, search):

    if start <= end:
        mid = int((start+end)/2)
        if arr[mid] == search:
            return mid
        elif search < arr[mid]:
            return bubble_sort(arr, start, mid-1, search)
        else:
            return bubble_sort(arr, mid+1, end, search)
    else:
        return -1


# Sorted array for binary search.
my_list = [10, 20, 30, 40, 50, 60, 70, 80, 90]

# search value.
key = 50

# get output of this program
result = bubble_sort(my_list, 0, len(my_list)-1, key)

# print output if found
if result != -1:
    print("Element found at index %d" % result)
else:
    print("Element are not found.")
