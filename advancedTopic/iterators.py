class PowThree:
    """Class to implement an iterator
        of powers of three"""

    def __init__(self, limit=0):
        self.limit = limit

    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n <= self.limit:
            result = 3 ** self.n
            self.n += 1
            return result
        else:
            raise StopIteration


my_num = PowThree(5)
my_iter = iter(my_num)
print(next(my_iter))
print(next(my_iter))
print(next(my_iter))
print(next(my_iter))
print(next(my_iter))
print(next(my_iter))
