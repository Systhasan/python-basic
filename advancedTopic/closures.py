def outer_func(n):
    def inner_func(x):
        print(n*x)
    return inner_func


# assign to inner_func() method to the inner_value variable.
inner_value = outer_func(1)
# assign to inner_func() method to the inner_value_2 variable.
inner_value_2 = outer_func(10)

# print inner method.
inner_value(5)
inner_value_2(8)
