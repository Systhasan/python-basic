#-------------------------------------------------------------------------------
# Name:        2D Dimensional List
# Purpose:
#
# Author:      Muhammad Hasan
#
# Created:     13/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    matrix = [
        [1,2,3],
        [4,5,6],
        [7,8,9]
    ]
    for row in matrix:
        for item in row:
            print(item, end='')
        print('')

if __name__ == '__main__':
    main()
