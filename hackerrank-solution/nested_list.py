n = int(input())

students = []

for i in range(n):
    student = [input(), float(input())]
    students.append(student)

first_min = second_min = 10000
index = 0
for std in students:
    if std[1] < first_min:
        second_min = first_min
        first_min = std[1]
        index = std[1]
    elif std[1] < second_min:
        second_min = std[1]
        index = std[1]

print(f'first min ', first_min)
print(f'second min ', second_min)

print(students)
