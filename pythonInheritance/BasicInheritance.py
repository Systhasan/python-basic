# Parent class or base class.
class Branch:

    def __init__(self, branch_name, address, number_emp, manager):
        self.branch_name = branch_name
        self.address = address
        self.number_emp = number_emp
        self.manager = manager

    def get_address(self):
        return self.address

    def get_manager(self):
        return self.manager

    def get_number_of_emp(self):
        return self.number_emp

    def get_branch_name(self):
        return self.branch_name


# Child class or derived class.
class Bank(Branch):

    def __init__(self, name, address, number_emp, manager):
        super().__init__(name, address, number_emp, manager)

    def get_branch_info(self):
        return f'''Branch Name: {self.get_branch_name()} 
        Address: {self.get_address()}
        Manager: {self.get_manager()}
        Number of Employee: {self.get_number_of_emp()}'''


# create child class object
dhaka_branch = Bank("Mirpur Branch", "Mirpur", "21", "Mr. Latif")
khulna_branch = Bank("Mongla Branch", "Mongla", "45", "Mr. Azad")

print(dhaka_branch.get_manager())
print(khulna_branch.get_branch_name())
print(dhaka_branch.get_branch_info())

