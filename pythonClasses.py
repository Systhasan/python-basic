#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Administrator
#
# Created:     13/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

class Point:
    def move(self):
        print('Move')

    def draw(self):
        print('Draw')

class Circle:
    _y=100
    # constructor...
    def __init__(self, x):
        self.x = x
    def getArea(self):
        return 2*3.1416*self.x**2

# create class instance.
point = Point()
# access class method.
point.draw();
# define class attribute.
point.x = 10
# get class attribute value.
print(point.x)

circle = Circle(10)
print(circle.getArea())
print(circle.x)
print(circle._y)

