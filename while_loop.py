#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Administrator
#
# Created:     13/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    command = ""
    while command != "quit":
        command = input(">> ").lower()
        if command == 'start':
            print('Car started ... ')
        elif command == 'stop':
            print('Car stopped.')
        elif command == 'help':
            print("""
            start - Start Car
            stop  - Stop car
            quit  - Quit from car
            """)
        else:
            print('Print valid command.')
if __name__ == '__main__':
    main()
