#-------------------------------------------------------------------------------
# Name:        Swap case
# Purpose:     Hacker Rank:
#              https://www.hackerrank.com/challenges/swap-case/problem
#
# Author:      Muhammad Hasan
#
# Created:     03/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main(s):
    string_list = list(s)
    str = []
    stri = ''
    # string_list = [ x.lower() for x in string_list ]

    for x in string_list:
        if x.isupper():
            str.append(x.lower())
            stri += x.lower()
        elif x.islower():
            str.append(x.upper())
            stri += x.upper()
        else:
            str.append(x)
            stri += x
    print(str)
    return stri

if __name__ == '__main__':
    s = input()
    result = main(s)
    print(result)
