#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Administrator
#
# Created:     14/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def kg_to_lbs(kg):
    return kg*2.2

def lbs_to_kg(lbs):
    return lbs*0.43
