#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Administrator
#
# Created:     29/06/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

class PyMethods:

    # class variable.
    class_variable = 'Class variable data.'

    # Constructor
    def __init__(self, var_1):
        self.var_1 = var_1

    def avg(self):
        return self.var_1


py_obj = PyMethods(100)
py_obj_2 = PyMethods(200)
print (py_obj.var_1)
print (py_obj_2.var_1)
print (py_obj_2.avg())