#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Administrator
#
# Created:     17/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from urllib import request

resp = request.urlopen("https://www.wikipedia.org")
print( type(resp) )  # --> class
print( resp.code )
print( resp.length )

data = resp.read()
print( type(data) ) # --> bytes
html = data.decode("UTF-8")
print( type(html) ) # --> str
print(html)
