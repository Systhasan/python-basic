#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Administrator
#
# Created:     25/06/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    n = int(input())
    student_marks = {}
    for _ in range(n):
        name, *line = input().split()
        scores = list(map(float, line))
        student_marks[name] = scores
        print(*line)
    query_name = input()
    print(student_marks)
    print( format(sum(student_marks[query_name]) / len(student_marks[query_name]), '.2f') )

if __name__ == '__main__':
    main()

