#-------------------------------------------------------------------------------
# Name:        Python Yield or Generator.
# Purpose:
#
# Author:      Administrator
#
# Created:     08/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def basicYield():
    yield 1;

def loopWithYield():
    yield 'A';
    yield 'B';
    yield 'C';

def numberSquare(i):
    yield i*i

if __name__ == '__main__':

   print(next(basicYield())) #--> output: 1

   print(loopWithYield()) #--> Will get a generator Object.

   print(list(loopWithYield())) #--> output: ['A','B','C']

   for x in loopWithYield():
    #print(x)    #--> Output: A B C

    myList = [1, 2, 3, 4, 5]
    # print( myList )

    for i in myList:
        print( next(numberSquare(i)) ) #--> output: 1 4 9 16 25




    # -------------------------------
    # Return sends a specified value back to its caller whereas Yield can produce a sequence of values.
    # We should use yield when we want to iterate over a sequence,
    # but don’t want to store the entire sequence in memory.
    # Yield are used in Python generators.
    # If the body of a def contains yield, the function automatically becomes a generator function.
    #
    #
    # URL: https://www.youtube.com/watch?v=bD05uGo_sVI
    # URL: https://djangostars.com/blog/list-comprehensions-and-generator-expressions/
    # -------------------------------
