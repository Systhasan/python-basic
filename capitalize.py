#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Administrator
#
# Created:     30/06/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    pass
    # take user input.
    value  = input()
    # make string to list.
    value  = value.split(" ")
    # Converting to capitalize.
    value  = [x.capitalize() for x in value]
    # mark list to stirng.
    output = ' '.join(value)
    return output

if __name__ == '__main__':
    main()
