#-------------------------------------------------------------------------------
# Name:        Unpacking
# Purpose:
#
# Author:      Muhammad hasan
#
# Created:     13/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    coordinates = [1,2,3]
    # here, the value will be: x=1, y=2, and z=3
    x, y, z = coordinates
    print(x, y, z)

if __name__ == '__main__':
    main()
