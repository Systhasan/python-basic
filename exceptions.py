#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Administrator
#
# Created:     13/07/2019
# Copyright:   (c) Administrator 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    try:
        age = int(input('Age: '))
        print(age)
    except ValueError:
        print('Invalid value')
    except ZeroDivisionError:
        print("Number can't by divided by 0")

if __name__ == '__main__':
    main()
